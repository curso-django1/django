from django.contrib import admin
from .models import Autor

@admin.register(Autor)
class AdminAutor(admin.ModelAdmin):
    
    list_display = [
        'name',
        'avatar',
        'description',
        'gender'
    ]
    
    class Meta:
        model = Autor
        
