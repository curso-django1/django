# Generated by Django 4.1 on 2022-08-27 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('autor', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='autor',
            name='gender',
            field=models.BooleanField(default=True, help_text='Genero'),
        ),
    ]
