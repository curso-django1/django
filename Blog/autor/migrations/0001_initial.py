# Generated by Django 4.1 on 2022-08-27 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Nombre', max_length=100)),
                ('avatar', models.ImageField(help_text='Avatar', upload_to='Autor')),
                ('description', models.TextField(help_text='Descripcion')),
            ],
        ),
    ]
