from django.db import models

class Autor(models.Model):
    
    name = models.CharField(
        max_length = 100,
        help_text = 'Nombre'
    )
    
    avatar = models.ImageField(
        upload_to = 'Autor',
        help_text = 'Avatar'
    )
    
    description = models.TextField(
        help_text = 'Descripcion'
    )
    
    gender = models.BooleanField(
        default = True,
        help_text = 'Genero'
    )
    
    def __str__(self):
        return self.name
