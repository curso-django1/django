from django.contrib import admin
from .models import Post

@admin.register(Post)
class AdminPost(admin.ModelAdmin):
    
    list_display = [
        'id_post',
        'title',
        'content',
        'date',
        'type',
        'status',
        'autor',
    ]
    
    class Meta:
        model = Post


