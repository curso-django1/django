from django.db import models
from autor.models import Autor

class Post(models.Model):
    
    id_post = models.AutoField(
        primary_key = True 
    )

    title = models.CharField(
        max_length = 100,
        help_text = 'Titulo'
    )
    
    content = models.TextField(
        help_text = 'Contenido'
    )
    
    date = models.DateTimeField(
        help_text = 'Fecha'
    )
   
    TECNOLOGIA = 'T'
    SALUD = 'S'
    ECONOMIA = 'E'
    DEPORTE = 'D'
    
    TYPE = [
        ('TECNOLOGIA', 'TECNOLOGIA'),
        ('SALUD', 'SALUD'),
        ('ECONOMIA', 'ECONOMIA'),
        ('DEPORTE', 'DEPORTE'),
    ]
    
    type = models.CharField(
        max_length = 10,
        choices = TYPE,
        default = TECNOLOGIA,
    )
    
    status = models.BooleanField(
        default = True
    )
    
    autor = models.ForeignKey(
        Autor, 
        on_delete = models.CASCADE
    )
    
    def __str__(self):
        return self.title
