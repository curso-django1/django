<div align="center">
  <a href="https://www.djangoproject.com/">
    <img src="https://res.cloudinary.com/due8e2c3a/image/upload/v1661718938/alfredynho/djangopony_thumb_2.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Curso Python/Django</h3>

  <p align="center">
    Curso de python/django
    <br />
  </p>
</div>


<details>
  <summary>Contenido</summary>
  <ol>
    <li>
      <a href="#about-the-project">Que es Django</a>
    </li>
    <li>
      <a href="#installation">Instalacion</a>
    </li>
    <li>
      <a href="#Contacts">Contactos</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Django (jang-goh) es un framework para aplicaciones
web gratuito y open source, escrito en Python. Es un
WEB framework - un conjunto de componentes que
te ayudan a desarrollar sitios web más fácil y
rápidamente.
una manera de manejarla autenticación de usuarios
(registrarse, iniciar sesión, cerrar sesión), un panel de
administración para tu sitio web,formularios, una
forma de subir archivos, etc.


<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Installation

_Consideraciones para levantar el proyecto de django._

1. Clonar el repositorio
   ```sh
   git clone 
   ```
2. crear el entorno virtual
   ```sh
   python -m virtualenv venv
   ```
3. Activar el entorno virtual
   ```js
   cd venv
   cd Scripts
   cd activate
   ```

4. Instalar Dependencias
   ```dp
   pip install -r requirements.txt
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contacts

Alfredo Callizaya - [@your_twitter](https://twitter.com/alfredynho) - alfredynho.cg@gmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>
